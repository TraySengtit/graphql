import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {ApolloProvider} from '@apollo/client';
import Countries, {client} from './src/screens/Countries';
import {NativeBaseProvider} from 'native-base';
const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <NativeBaseProvider>
        <ApolloProvider client={client}>
          <Countries />
        </ApolloProvider>
      </NativeBaseProvider>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
