import { ApolloClient, InMemoryCache, ApolloProvider, useQuery, gql } from '@apollo/client';
import React, { useState } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { VStack, Checkbox, Center, Text, HStack } from 'native-base';
import CenterSpinner from '../components/CenterSpinner';

export const client = new ApolloClient({
    uri: 'https://countries.trevorblades.com/',
    cache: new InMemoryCache(),
});

const CuntriesQuery = () => {
    const [country, setCountry] = useState(true);
    const [code, setCode] = useState(true);
    const [emoji, setEmoji] = useState(true);
    const [phone, setPhone] = useState(true);
    const [lang, setLang] = useState(true);

    const COUNTRIESQUERY = gql`
    query openQuery($country: Boolean!, $code: Boolean!, $emoji: Boolean!, $phone: Boolean!, $lang: Boolean!){
        countries{
        name @include(if: $country)
        code @include(if: $code)
        emoji @include(if: $emoji)
        phone @include(if: $phone)
        languages{
            name @include(if: $lang)
            }
        }
    }
    `;

    const { data, error, loading } = useQuery(COUNTRIESQUERY, {
        variables: {
            country: country,
            code: code,
            phone: phone,
            emoji: emoji,
            lang: lang
        }
    });
    if (loading) {
        return <CenterSpinner />;
    }
    if (error) {
        return <Text>Loading Error !!</Text>;
    }

    const myData = data.countries;
    return (
        
        <View style={styles.container}>
                
            <VStack space={3} alignItems="flex-start">
            <Text fontSize='2xl'>Select Options</Text>
                <Checkbox value="country" colorScheme="danger" defaultIsChecked onChange={()=> setCountry(!country)}>
                    Country
                </Checkbox>
                <Checkbox value="phone" colorScheme="info" defaultIsChecked onChange={()=> setPhone(!phone)}>
                    Phone
                </Checkbox>
                <Checkbox value="code" colorScheme="orange" defaultIsChecked onChange={()=> setCode(!code)}>
                    Code
                </Checkbox>
                <Checkbox value="lang" colorScheme="purple" defaultIsChecked onChange={()=> setLang(!lang)}>
                    Language
                </Checkbox>
                <Checkbox value="emoji" colorScheme="yellow" defaultIsChecked onChange={()=> setEmoji(!emoji)}>
                    Emoji
                </Checkbox> 
                <Text fontSize='2xl'>List</Text>
            </VStack>
            <ScrollView
                style={styles.scrollView}
                contentContainerStyle={styles.scrollViewContainer}>   
                {myData.map((item, key) =>
                <>
                    <View key={key} style={{padding: 10}}>
                        <HStack><Text>{key + 1}</Text></HStack>
                        <VStack>
                            {country === true ? <Text>Country: {item.name}</Text> : null}
                            {phone === true ? <Text>Phone: {item.phone}</Text> : null}
                            {code === true ? <Text>Code: {item.code}</Text> : null}
                            {lang === true ? <Text>Language: {item.languages[0]?.name}</Text> : null}
                            {emoji === true ? <Text>Emoji: {item.emoji}</Text> : null}
                        </VStack>
                    </View>
                </>
                )}
            </ScrollView>
        </View>
    );
};

export const Countries = () => {
    return (
        <View style={styles.container}>
            <Center><Text fontWeight='bold' fontSize='3xl'>List Of Countries</Text></Center>
            <CuntriesQuery />
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginLeft: 10
    },
    scrollView: {
        flex: 0.8,
        paddingTop: 10,
        paddingHorizontal: 10
    },
    scrollViewContainer: {
        justifyContent: 'flex-start'
    },
    text: {
        fontSize: 12
    },
});

export default Countries;
