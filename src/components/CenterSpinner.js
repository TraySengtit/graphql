import React from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  Text
} from 'react-native';

const CenterSpinner = () => (
  <View style={styles.container}>
    <ActivityIndicator />
    <Text>Please wait...</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});


export default CenterSpinner;